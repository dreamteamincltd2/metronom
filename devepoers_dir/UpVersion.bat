javac -sourcepath ./src -d out/production/Metronome src/Main.java
echo main-class: Main>manifest.mf
echo class-path: out/artifacts/Metronome_jar/Metronome.jar >>manifest.mf
jar -cmf manifest.mf out/artifacts/Metronome_jar/Metronome.jar -C out/production/Metronome .
java -jar VersionUpdater.jar out/artifacts/Metronome_jar/version.txt
iscc setupscript/SetupScript.iss
git pull
git commit -a -m "UpVersion"
git push