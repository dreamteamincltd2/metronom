to run a metronome from console:
java -jar Metronome.jar b s_d1 s_d2 tempo num_t

b: mode. 0 - is usual, 1 - is advanced
s_d1 s_d2: size. s_d1/s_d2. 
tempo: Tempo.
num_t: Quantity of tacts.

Examples:
java -jar Metronome.jar 0 4 4 110 1
Will endlessly play a 4/4 in tempo 110

java -jar Metronome.jar 1 4 4 110 5 5 4 100 5
Will play five tacts of 4/4 in teempo 110 and then five tacts of 5/4 in tempo 100.