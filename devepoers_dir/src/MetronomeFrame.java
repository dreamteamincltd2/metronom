import javax.swing.*;

/**
 * Created by Dan on 25.10.2014.
 */
public class MetronomeFrame extends JFrame {
    public MetronomeFrame() {
        setTitle("Metronome");
        setSize(600, 300);
        MetronomePanel panel = new MetronomePanel();
        add(panel);
        setLocation(500, 500);
        //pack();
    }
}
