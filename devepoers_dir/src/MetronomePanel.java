import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by Dan on 25.10.2014.
 */
public class MetronomePanel extends JPanel {
    private JComboBox <String> note;
    private int intnote;
    private JComboBox <String> quant;
    private int intquant;
    private int quantityt;
    private JComboBox <String> tempo;
    private JComboBox <String> quantity_t;
    private int inttempo;
    private JButton playstop;
    private JPanel modepanel;
    private JLabel modeselescted;
    private JButton modechanger;
    private SequenceObject o;
    private boolean isplayed;
    private MidiHandler player;
    private Boolean isextended;
    private JButton addbutton;
    private JPanel custom;
    private LinkedList<SequenceObject> clickmelody;
    private JTextArea textAr;
    private JScrollPane scrl;
    private JButton deletelast;
    private JButton clear;
    private JButton open;
    private SaverLoader sv;
    private JButton save;
    private JTextField savename;


    public MetronomePanel() {
        sv = new SaverLoader();
        open = new JButton("Open");
        savename = new JTextField("Enter Name", 12);
        custom = new JPanel();
        player = new MidiHandler();
        clickmelody = new LinkedList<SequenceObject>();
        deletelast = new JButton("Delete Last");
        textAr = new JTextArea(8, 40);
        scrl = new JScrollPane(textAr);
        textAr.setEditable(false);
        textAr.setEnabled(false);
        isplayed = false;
        isextended = false;
        addbutton = new JButton("Add");
        addbutton.addActionListener(new Adder());
        deletelast.addActionListener(new DeleteLast());
        clear = new JButton("Clear");
        save = new JButton("Save");
        save.addActionListener(new Saver());
        clear.addActionListener(new Clearer());
        clear.setEnabled(false);
        addbutton.setEnabled(false);
        deletelast.setEnabled(false);
        modechanger = new JButton("Switch to Extended Mode");
        modechanger.addActionListener(new ModeChListener());
        modeselescted = new JLabel("Standart mode");
        modepanel = new JPanel();
        note = new JComboBox<String>();
        quant = new JComboBox<String>();
        tempo = new JComboBox<String>();
        quantity_t = new JComboBox<String>();
        playstop = new JButton("Play");
        note.setEditable(false);
        quant.setEditable(false);
        note.addItem("1");
        note.addItem("2");
        note.addItem("4");
        note.addItem("8");
        note.addItem("16");
        note.addItem("32");
        note.addItem("64");
        note.setSelectedIndex(2);
        for(int i = 1; i < 65; i++) {
            quant.addItem(Integer.toString(i));
        }
        quant.setSelectedIndex(3);
        for(int i = 10; i < 301; i ++) {
            tempo.addItem(Integer.toString(i));
        }
        tempo.setSelectedIndex(100);
        for(int i = 1; i < 301; i++){
            quantity_t.addItem(Integer.toString(i));
        }
        quantityt = 1;
        quantity_t.addActionListener(new quantTListener());
        note.addActionListener(new noteListener());
        quant.addActionListener(new quantListener());
        tempo.addActionListener(new tempoListener());
        playstop.addActionListener(new standartListener());
        modepanel.add(modeselescted);
        modepanel.add(quant);
        modepanel.add(new JLabel("/"));
        modepanel.add(note);
        modepanel.add(new JLabel("Tempo: "));
        modepanel.add(tempo);
        modepanel.add(new JLabel("Quantity of tackts: "));
        modepanel.add(quantity_t);
        quantity_t.setEnabled(false);
        open.addActionListener(new Opener());
        savename.setEditable(true);
        custom.add(addbutton, BorderLayout.NORTH);
        custom.add(deletelast, BorderLayout.CENTER);
        custom.add(clear, BorderLayout.SOUTH);
        custom.add(open, BorderLayout.WEST);
        custom.add(save, BorderLayout.SOUTH);
        custom.add(savename, BorderLayout.SOUTH);
        add(modechanger, BorderLayout.NORTH);
        add(modepanel, BorderLayout.CENTER);
        add(playstop, BorderLayout.SOUTH);
        add(custom, BorderLayout.EAST);
        add(textAr, BorderLayout.SOUTH);
        textAr.add(scrl);
        textAr.setVisible(false);
        open.setEnabled(false);
        save.setEnabled(false);
        savename.setEnabled(false);
        intnote = Integer.parseInt((String)note.getSelectedItem());
        intquant = Integer.parseInt((String)quant.getSelectedItem());
        inttempo = Integer.parseInt((String)tempo.getSelectedItem());
    }
    private void fillTextAr() {
        textAr.setText("Your sequence listed below:\n");
        ListIterator<SequenceObject> it = clickmelody.listIterator(clickmelody.size());
        while (it.hasPrevious()) {
            o = it.previous();
            textAr.append(o.toString() + "\n");
        }
    }

    private class standartListener implements ActionListener{
        Thread t;
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!isplayed) {
                if(isextended && clickmelody.size() > 0) {
                    playstop.setText("Stop");
                    isplayed = true;
                    PlayRunneble r = new PlayRunneble();
                    t = new Thread(r);
                    t.setPriority(Thread.MAX_PRIORITY);
                    t.start();
                    note.setEnabled(false);
                    tempo.setEnabled(false);
                    quant.setEnabled(false);
                }
                else if (!isextended) {
                    playstop.setText("Stop");
                    isplayed = true;
                    o = new SequenceObject(intquant, intnote, inttempo, 1);
                    PlayRunneble r = new PlayRunneble();
                    t = new Thread(r);
                    t.setPriority(Thread.MAX_PRIORITY);
                    t.start();
                    note.setEnabled(false);
                    tempo.setEnabled(false);
                    quant.setEnabled(false);
                }
                else {

                }
            }
            else if(isplayed){
                playstop.setText("Play");
                isplayed = false;
                t.stop();
                player.stopSequence();
                note.setEnabled(true);
                tempo.setEnabled(true);
                quant.setEnabled(true);
            }


        }
    }

    private class noteListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            intnote = Integer.parseInt((String)note.getSelectedItem());
        }
    }

    private class quantListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            intquant = Integer.parseInt((String)quant.getSelectedItem());
        }
    }

    private class tempoListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            inttempo = Integer.parseInt((String)tempo.getSelectedItem());
        }
    }

    private class quantTListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            quantityt = Integer.parseInt((String)quantity_t.getSelectedItem());
        }
    }

    private class ModeChListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!isextended) {
                modechanger.setText("Switch to Standart Mode");
                modeselescted.setText("Extended Mode");
                quantity_t.setEnabled(true);
                addbutton.setEnabled(true);
                textAr.setEnabled(true);
                deletelast.setEnabled(true);
                clear.setEnabled(true);
                textAr.setVisible(true);
                open.setEnabled(true);
                savename.setEnabled(true);
                save.setEnabled(true);
                isextended = true;

            }
            else{
                modechanger.setText("Switch to Extended Mode");
                modeselescted.setText("Standart Mode");
                custom.setEnabled(false);
                quantity_t.setEnabled(false);
                addbutton.setEnabled(false);
                textAr.setEnabled(false);
                textAr.setVisible(false);
                deletelast.setEnabled(false);
                clear.setEnabled(false);
                open.setEnabled(false);
                savename.setEnabled(false);
                save.setEnabled(false);
                isextended = false;
            }

        }
    }
    private class Adder implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            clickmelody.push(new SequenceObject(intquant, intnote, inttempo, quantityt));
            fillTextAr();
        }
    }
    private class DeleteLast implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(clickmelody.size() > 0) {
                clickmelody.removeFirst();
                fillTextAr();
            }
        }
    }
    private class Clearer implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            clickmelody.clear();
            fillTextAr();
        }
    }

    private class PlayRunneble implements Runnable{
        @Override
        public void run() {
            try {
                if(isextended) {
                   player.playSequenceSequence(clickmelody);
                }
                else {
                    while (isplayed) {
                        player.playSequence(o);
                    }
                }
                while(player.isPlaying()) {

                }
                playstop.setText("Play");
                isplayed = false;
                player.stopSequence();
                note.setEnabled(true);
                tempo.setEnabled(true);
                quant.setEnabled(true);
            }
            catch(InterruptedException ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    private class Opener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            OpenFrame p = new OpenFrame(sv.getFolder());
            p.setVisible(true);
            p.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
    }
    private class Saver implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(savename.getText().contains(" ")) {
                savename.setText("Wrong Name");
            }
            else if(clickmelody.size() < 1) {
                savename.setText("Nothing to save");
            }
            else {
                sv.saveSequence(savename.getText(), clickmelody);
                savename.setText(savename.getText() + " saved");
            }

        }
    }

    private class OpenFrame extends JFrame {
        private JButton cancel;
        private JButton open;
        private JComboBox <String> files_list;
       public OpenFrame(String[] files) {
           setTitle("Open");
           setSize(100, 100);
           if (files != null) {
               files_list = new JComboBox<String>();
               for (String f : files) {
                   files_list.addItem(f);
               }
               add(files_list, BorderLayout.NORTH);
               open = new JButton("Open selected");
               open.addActionListener(new Op());
               add(open, BorderLayout.CENTER);
           }
           else {
               add(new JLabel("No Saved Files"));
           }
           cancel = new JButton("Cancel");
           cancel.addActionListener(new Canceler());
           add(cancel, BorderLayout.SOUTH);
           pack();
           setLocation(550, 550);
       }
        private class Canceler implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                closeOpener();
            }
        }
        private class Op implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                clickmelody = sv.loadSequnce((String)files_list.getSelectedItem());
                fillTextAr();
                closeOpener();
            }
        }
        private void closeOpener() {
            this.dispose();
        }
    }



}
