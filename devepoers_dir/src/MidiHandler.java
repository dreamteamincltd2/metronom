import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by Dan on 13.10.2014.
 */
public class MidiHandler {
    private Sequence sequence;
    private Sequencer sequencer;
    private Sequence sequenceS;
    private Sequencer sequencerS;

    public MidiHandler() {
        try {
            sequence = MidiSystem.getSequence(new File("snare.mid"));
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.setSequence(sequence);
            sequenceS = MidiSystem.getSequence(new File("strong.mid"));
            sequencerS = MidiSystem.getSequencer();
            sequencerS.open();
            sequencerS.setSequence(sequenceS);

            sequencer.addMetaEventListener(new MetaEventListener() {
                @Override
                public void meta(MetaMessage meta) {
                   /* if(x == 9) {
                       return;
                    }*/
                }
            });


        }
        catch (IOException e) {
            System.out.println(e.getMessage());

        } catch (MidiUnavailableException e) {
            System.out.println(e.getMessage());
        } catch (InvalidMidiDataException e) {
            System.out.println(e.getMessage());
        }
        sequencer.start();
        sequencerS.start();
        /*try {
            Thread.sleep(100);
        }
        catch (InterruptedException e) {

        }*/

    }
    public void playSequence(SequenceObject o) throws InterruptedException {
        int quantity = o.getQuantity_of_tacts();
        int quantity_of_ticks = o.getSize_devident();
        long wait = o.getWait_n();
        for(int i = 0; i < quantity; i++) {
            for(int j = 0; j < quantity_of_ticks; j++) {
                if(j == 0) {
                    sequencerS.setTickPosition(0);
                    sequencerS.start();
                }
                else {
                    sequencer.setTickPosition(0);
                    sequencer.start();
                }
                Thread.sleep(wait);
            }
        }
        return;
    }
    public void playSequenceSequence(LinkedList<SequenceObject> l) throws InterruptedException {
        SequenceObject o;
        ListIterator<SequenceObject> it = l.listIterator(l.size());
        while (it.hasPrevious()) {
            o = it.previous();
            int quantity = o.getQuantity_of_tacts();
            int quantity_of_ticks = o.getSize_devident();
            long wait = o.getWait_n();
            for(int i = 0; i < quantity; i++) {
                for(int j = 0; j < quantity_of_ticks; j++) {
                    if(j == 0) {
                        sequencerS.setTickPosition(0);
                        sequencerS.start();
                    }
                    else {
                        sequencer.setTickPosition(0);
                        sequencer.start();
                    }
                    Thread.sleep(wait);
                }
            }
        }
    }
    public void stopSequence() {
        sequencer.stop();
        sequencer.setTickPosition(0);
    }
    public boolean isPlaying() {
        return sequencer.isRunning();
    }

}
