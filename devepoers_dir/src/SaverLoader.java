import java.io.*;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by Dan on 05.11.2014.
 */
public class SaverLoader {
    public SaverLoader() {
    }

    public LinkedList<SequenceObject> loadSequnce(String name) {
        int note = 0;
        int quant = 0;
        int tempo = 0;
        int quantity_of_tackts = 0;
        int i = 0;
        LinkedList<SequenceObject> l  = new LinkedList<SequenceObject>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("saved/" + name));
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.contains("end")) {
                        break;
                    }
                    if(i == 0) {
                        note = Integer.parseInt(line);
                        i++;
                    }
                    else if(i == 1) {
                        quant = Integer.parseInt(line);
                        i++;
                    }
                    else if(i == 2) {
                        tempo = Integer.parseInt(line);
                        i++;
                    }
                    else if(i == 3) {
                        quantity_of_tackts = Integer.parseInt(line);
                        l.push(new SequenceObject(note, quant, tempo, quantity_of_tackts));
                        i = 0;
                    }

                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch(FileNotFoundException e){
                e.printStackTrace();
        }
        if(l.size() > 0) {
            return l;
        }
        return null;
    }

    public void saveSequence(String name, LinkedList<SequenceObject> l){
        try {
            PrintWriter out = new PrintWriter(new File("saved/" + name + ".mtn").getAbsoluteFile());
            ListIterator<SequenceObject> it = l.listIterator(l.size());
            SequenceObject o;
            while (it.hasPrevious()) {
                o = it.previous();
                int quantity = o.getSize_devident();
                int note = o.getSize_devisor();
                int tempo = o.getTempo();
                int quant_of_tacts = o.getQuantity_of_tacts();
                out.println(quantity);
                out.println(note);
                out.println(tempo);
                out.println(quant_of_tacts);

            }
           /* for(SequenceObject o : l) {
                int quantity = o.getSize_devident();
                int note = o.getSize_devisor();
                int tempo = o.getTempo();
                int quant_of_tacts = o.getQuantity_of_tacts();
                out.println(quantity);
                out.println(note);
                out.println(tempo);
                out.println(quant_of_tacts);
            }*/
            out.print("end");
            out.close();
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }


    }

    public String[] getFolder() {
        File folder = new File("saved/");
        String[] files = folder.list(new FilenameFilter() {
            @Override
            public boolean accept(File folder, String name) {
                return name.endsWith(".mtn");
            }
        });

        return files;
    }
}
