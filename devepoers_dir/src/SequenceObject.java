/**
 * Created by Dan on 19.10.2014.
 */
public class SequenceObject {
    private int tempo;
    private final int minute = 60000;
    private final int devisor = 4;
    private long wait_n;
    private int size_devident;
    private int size_devisor;
    private int actual_devisor;
    private int quantity_of_tacts;
    public SequenceObject(int size1, int size2, int temp, int quan ) {
        quantity_of_tacts = quan;
        size_devident = size1;
        size_devisor = size2;
        tempo = temp;
        actual_devisor = (size_devisor/ devisor) * tempo;
        wait_n = minute / actual_devisor;
    }
    public String toString() {
        return Integer.toString(size_devident) + "/" + Integer.toString(size_devisor)
                + " Tempo: " + Integer.toString(tempo) + " Quantity: " + Integer.toString(quantity_of_tacts);
    }

    public long getWait_n() {
        return wait_n;
    }

    public int getQuantity_of_tacts() {
        return quantity_of_tacts;
    }
    public int getSize_devident() {
        return size_devident;
    }
    public int getSize_devisor() {return size_devisor;}
    public int getTempo() {return tempo;}


}
